import {DynamoDBClient} from "@aws-sdk/client-dynamodb"
import * as membersController from "./controller/update-controller.js"

export const handler = async (event) => {
    const config = {tableName: "Members"}
    let statusCode, body, member, lastName, email;
    try {
        const client = new DynamoDBClient({});
        const deps = {config, client};
        lastName = event.queryStringParameters.lastName;
        email = event.queryStringParameters.email;
        member = JSON.parse(event.body);

        const result = await membersController.updateMember(
            deps, member, email, lastName
        );
        statusCode = result.statusCode
        body = JSON.stringify(result.body)
    } catch (err) {
        statusCode = 500;
        body = err.message;
    }
    return {
        "headers": {"content-type":"application/json"},
        "isBase64Encoded": false,
        statusCode,
        body,
    }
};
