import { memberSchema } from "../util/member-schema.js";
import * as dbClient from "../db/members.js";

export const updateMember = async (deps, member, email, lastName) => {
    const validationResult = memberSchema.validate(member, {
        abortEarly: false,
    });
    if (validationResult?.error || !email || !lastName) {
        return {
            statusCode: 400,
            body: { message: validationResult?.error?.message || "email and lastName are required" },
        };
    }

    const updateResult = await dbClient.updateMember(deps, member, email, lastName);
    if (!updateResult.ok) {
        return { statusCode: 500, message: updateResult.message };
    }
    return { statusCode: 200, body: { memberUpdates: updateResult.updates } };
};
