import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import {
    updateMember,
} from "./update-controller.js";
import {createMember} from "./../../functionWrite/db/members"
import { configuration } from "../../config.js";
import { fetchRandomUser } from "./../util/random-user.js";

describe("update controller test suite", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
    it("should update the gender of a member", async () => {
       const member=await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            member
        );
        expect(result).toBeTruthy();

        const getResult = await updateMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            {
                ...member,
                gender: "female",
            }, member.email, member.lastName
        );
        expect(getResult.statusCode).toBe(200);
        expect(getResult.body.memberUpdates.gender).toBe("female");
    });

    it("should return 400:bad-request when email, last name, or gender is not provided", async () => {
        const getResult = await updateMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            {}, "", ""
        );

        expect(getResult.statusCode).toBe(400);
        expect(getResult.body.message).toBe(
            "email and lastName are required"
        );
    });
})
