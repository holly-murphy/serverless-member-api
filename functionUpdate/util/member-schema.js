import Joi from "joi";

const genderEnum = ["male", "female", "na"];
const lastNameSchema = Joi.string().allow(null,"");
const emailSchema = Joi.string()
    .email({ tlds: { allow: false } }).allow(null,"");
const genderSchema = Joi.string()
    .valid(...genderEnum)
    .insensitive().allow(null, "");

export const memberSchema = Joi.object({
    firstName: Joi.string().allow(null, ""),
    lastName: lastNameSchema,
    middleInitial: Joi.string().allow(null, ""),
    phone: Joi.string().allow(null, ""),
    email: emailSchema,
    gender: genderSchema
});