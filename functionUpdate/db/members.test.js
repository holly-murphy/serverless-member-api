import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { configuration } from "./../config";
import { createMember } from "./../../functionWrite/db/members";
import { fetchRandomUser } from "../util/random-user";
import {updateMember} from "./members"

describe("members db client - update - tests suite: ", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
const  config={
        ...configuration("dev"),
        tableName: "test",
    }
    it("should update a member's gender to male", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config,
            },
            member
        );
        expect(result.ok).toBeTruthy();

        const updateResult = await updateMember(
            {
                client: dbClient,
                config,
            },
            { ...member, gender: "male" }, member.email, member.lastName
        );
        expect(updateResult.ok).toBe(true);
        expect(updateResult.updates.gender).toBe("male");
    });

    it("should update a member's the first name and phone", async () => {
        const result = await createMember(
            {
                client: dbClient,
                config
            },
            { firstName: "test-first", lastName: "test-last", phone: '1231231234', email: "testing@sometest2.com", gender: "female"},
        );

        const updateResult = await updateMember(
            {
                client: dbClient,
                config
            },
            {  firstName: "new-test-first", lastName: "test-last",middleInitial: 't', phone: '7581223444', gender: "female" }, "testing@sometest2.com", "test-last"
        );
        expect(updateResult.ok).toBe(true);
        expect(updateResult.updates.firstName).toBe("new-test-first");
        expect(updateResult.updates.phone).toBe('7581223444')
    });


    it("should return ok: false with a reason of 'db-error' if unable to fetch a member to update", async () => {
        const updateResult = await updateMember(
            {
                client: dbClient,
                config
            },
            { gender: "male", lastName:"something" }, "notfound", "last-name"
        );

        expect(updateResult.ok).toBe(false);
        expect(updateResult.reason).toBe("db-error");
        expect(updateResult.message).toBeDefined();
    });

    it("should return ok: false with a reason if something bad happens during fetch", async () => {
        const updateResult = await updateMember(
            {
                client: dbClient,
                config
            },
            { gender: "male", email: "not-legit" }
        );

        expect(updateResult.ok).toBe(false);
        expect(updateResult.reason).toBe("db-error");
        expect(updateResult.message).toBeDefined();
    });
});
