import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";

export const updateMember = async (deps, memberDetails, email, lastName) => {
    try {
        const documentDb = DynamoDBDocument.from(deps.client);
        const params = {
            TableName: deps.config.tableName,
            Key: {
                email: email,
                lastName: lastName
            },
            ConditionExpression: "email = :e and lastName = :ln",
            UpdateExpression: "set gender = :g, firstName = :fn, phone = :p, middleInitial = :mi",
            ExpressionAttributeValues: {
                ":g": memberDetails.gender || "",
                ":e": email,
                ":fn": memberDetails.firstName || "",
                ":ln": lastName,
                ":p": memberDetails.phone || "",
                ":mi": memberDetails.middleInitial || ""
            },
            ReturnValues: "UPDATED_NEW",
        };
        const updateResult = await documentDb.update(params);

        if (
            updateResult.$metadata.httpStatusCode === 200 &&
            Object.keys(updateResult?.Attributes).length > 0
        ) {
            return {ok: true, updates: updateResult?.Attributes};
        }
        return {
            ok: false,
            reason: "no-match",
            message: "Unable to update gender.",
        };
    } catch (e) {
        console.log(`update was unsuccessful: `);
        console.log(e);
        return {ok: false, reason: "db-error", message: e.message};
    }
}