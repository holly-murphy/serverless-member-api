const tableName = "Members";

const awsLocalConfig = {
    //Provide details for local configuration
    accessKeyId: "local",
    secretAccessKey: "local",
    region: "local",
};
const awsRemoteConfig = {
    accessKeyId: "ACCESS_KEY_ID",
    secretAccessKey: "SECRET_ACCESS_KEY",
    region: "us-east-1",
};
const dbParams = {
    TableName: "members",
    KeySchema: [
        { AttributeName: "email", KeyType: "HASH" },
        { AttributeName: "lastName", KeyType: "RANGE" },
    ],
    AttributeDefinitions: [
        { AttributeName: "lastName", AttributeType: "S" },
        { AttributeName: "email", AttributeType: "S" },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 50,
        WriteCapacityUnits: 50,
    },
};

export const configuration = (env) => {
    switch (env) {
        case "dev":
            return {
                tableName,
                awsConfig: awsLocalConfig,
                dbParams,
                region: "local",
                dbEndpoint: "localhost:8000"
            };
        case "prod":
            return {
                tableName,
                awsConfig: awsRemoteConfig,
                dbParams,
                region: "us-east-1",
            };
        default:
            return {
                tableName,
                awsConfig: awsLocalConfig,
                dbParams,
                region: "local",
            };
    }
};
