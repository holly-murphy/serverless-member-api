import {DynamoDBClient, QueryCommand} from "@aws-sdk/client-dynamodb";
import { createMember } from "./../../functionWrite/db/members"
import { fetchRandomUser } from "../util/random-user";

describe("members db client - create - tests suite: ", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
    const tableName="test";

    it("should add a new member", async () => {
        const member = await fetchRandomUser();
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    tableName: `test`,
                },
            },
            member
        );
        expect(result.ok).toBeTruthy();
    });

    it("should return ok: false with a reason of 'db-error' if unable to add a member due to bad request", async () => {
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    tableName: `test`,
                },
            },
            {}
        );

        expect(result.ok).toBe(false);
        expect(result.reason).toBe("db-error");
        expect(result.message).toBeDefined();
    });
});
