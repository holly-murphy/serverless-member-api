import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";

export const createMember = async (deps, member) => {
    try {
        const documentDb = DynamoDBDocument.from(deps.client);
        const createResult = await documentDb.put({
            TableName: deps.config.tableName,
            ConditionExpression: "attribute_not_exists(email)",
            Item: { ...member },
        });

        if (createResult.$metadata.httpStatusCode === 200) {
            return { ok: true };
        }
        return { ok: false, reason: "fail", message: "Member could not be added." };
    } catch (e) {
        console.log(`creation of a new member was unsuccessful: `);
        console.log(e);
        return { ok: false, reason: "db-error", message: e.message === 'The conditional request failed' ? "Member with this email already exists." : e.message };
    }
};