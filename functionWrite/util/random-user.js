import axios from "axios";

export const fetchRandomUser = async () => {
    const random = await axios.get("https://randomuser.me/api");
    if (random.status === 200) {
        const randomUser = random.data.results[0];
        return {
            email: randomUser.email || "",
            phone: randomUser.phone || "",
            firstName: randomUser.name.first || "",
            lastName: randomUser.name.last || "",
            gender: randomUser.gender || "",
            middleInitial: "",
        };
    }
    return { ok: false };
};
