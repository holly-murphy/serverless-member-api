import {DeleteTableCommand, DynamoDBClient} from "@aws-sdk/client-dynamodb";
import {createMember} from "./write-controller.js";
import { configuration } from "../../config.js";
import { fetchRandomUser } from "./../util/random-user.js";

describe("create controller test suite", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
    it("should create a member", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            member
        );

        expect(result.statusCode).toBe(201);
        expect(result.body.member).toBe(member);
    });

    it("should return a 400:bad-request if required fields are missing and email is not valid", async () => {
        const localMember = { email: "not a valid email" };
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            { ...localMember }
        );
        expect(result.statusCode).toBe(400);
        expect(result.body.message).toBe(
            '"lastName" is required. "email" must be a valid email'
        );
    });



   });
