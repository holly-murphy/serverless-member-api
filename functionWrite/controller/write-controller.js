import { memberSchema } from "../util/member-schema.js";
import * as dbClient from "../db/members.js";

export const createMember = async (deps, member) => {
    const validationResult = memberSchema.validate(member, { abortEarly: false });
    if (validationResult?.error) {
        return {
            statusCode: 400,
            body: { message: validationResult.error.message },
        };
    }
    const createResult = await dbClient.createMember(deps, member);
    if (!createResult.ok) {
        return { statusCode: 500, body: { message: createResult.message } };
    }
    return { statusCode: 201, body: { member } };
};
