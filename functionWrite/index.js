import {DynamoDBClient} from "@aws-sdk/client-dynamodb"
import * as membersController from "./controller/write-controller.js"

export const handler = async (event) => {
    const config = {tableName: "Members"}
    let statusCode, body, member;
    try {
        const client = new DynamoDBClient({});
        const deps = {config, client};
        member = JSON.parse(event.body);
        const result = await membersController.createMember(
            deps,
            member
        );
        statusCode = result.statusCode
        body = JSON.stringify(result.body)
    } catch (err) {
        statusCode = 500;
        body = err;
    }
    return {
        "headers": {"content-type":"application/json"},
        "isBase64Encoded": false,
        statusCode,
        body,
    }
};
