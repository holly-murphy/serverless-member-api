import {
    DynamoDBClient,
    ListTablesCommand,
    CreateTableCommand,
} from "@aws-sdk/client-dynamodb";

export class DB {
    client;

    constructor(client) {
        this.client = client;
    }

    static async buildDb(config) {
        console.log(`building Db for dbEndpoint: ${config.dbEndpoint}...`);
        const client = new DynamoDBClient({});

        const existingTables = await client.send(new ListTablesCommand({}));
        if (!existingTables.TableNames.includes(config.tableName)) {
            console.log(
                `no existing ${config.tableName} table, so generating the table...`
            );
            await client.send(
                new CreateTableCommand({
                    ...config.dbParams,
                    TableName: config.tableName,
                })
            );
        }
        return new DB(client);
    }
}
