# serverless-member-api
CRUD API served up via AWS API Gateway, 4 lambdas, and dynamoDb.

Each 'member' is considered unique by email and last name (keys). At current time, email and last name can not be updated with the PUT.

-----
Documentation can be found [here](https://galactic-crescent-427837.postman.co/workspace/03865c29-bc32-4826-81d7-f4a896e932eb/api/279c2b78-edb2-4597-a3b5-0908dd5bfd06/version/cf43c222-44e6-4287-9224-93cb144e22fc?tab=documentation).

To run tests via postman:
1. navigate to [postman](https://galactic-crescent-427837.postman.co/workspace/03865c29-bc32-4826-81d7-f4a896e932eb/api/279c2b78-edb2-4597-a3b5-0908dd5bfd06/version/cf43c222-44e6-4287-9224-93cb144e22fc?tab=test)
2. select or verify the ```STAGE : serverless-member-api``` environment is selected
3. hover over the serverless-member-api row
4. select ```Run Test``` 

----

To run locally:  

Start the SAM local API (depends on [SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html) and Docker being installed).  
1. clone this repo
2. run ``` sam build ```
3. run ``` sam local start-api ```

You should now be able to hit the endpoints locally.

-----  

To run the tests, change directories into the function you wish to run the test for and:
1. run ```npm i``` to install the deps
2. run ```npm run test```   
*NOTE: this current depends on dynamoDB being available locally   

To run dynamodb locally,
```
run docker-compose up from ./local
```
To list tables:
```
aws dynamodb list-tables --endpoint-url http://localhost:8000 
```

To delete a table:
```
aws dynamodb delete-table --endpoint-url http://localhost:8000 --table-name members
```  

To list all items in a table:
```
aws dynamodb scan --endpoint-url http://localhost:8000 --table-name Members    
```

-----
Open items that remain in progress:
1. add logger and turn on cloudwatch logs
2. add auth
3. add lint, commintlint, husky
5. mock out unit tests to break the dep on a locally running dynamoDb
6. handle casing (currently case-sensitive)
