import {configuration} from "./../config.js";
import {DB} from "./../DB.js"
import {CreateTableCommand, DynamoDBClient, ListTablesCommand} from "@aws-sdk/client-dynamodb";

export default async()=>{
    console.log("setting up for all tests...")
    const config = configuration("dev")
    const params = {
        region: "test",
        dbEndpoint: "http://localhost:8000",
        dbParams: config.dbParams,
        tableName: `test`,
    }

    console.log(`building Db for dbEndpoint: ${params.dbEndpoint}...`);
    const client = new DynamoDBClient({endpoint: params.dbEndpoint,});

    const existingTables = await client.send(new ListTablesCommand({}));
    if (!existingTables.TableNames.includes(params.tableName)) {
        console.log(
            `no existing ${params.tableName} table, so generating the table...`
        );
        await client.send(
            new CreateTableCommand( {...params.dbParams, TableName: "test"})
        );
    }
    console.log("database should be available...")

}