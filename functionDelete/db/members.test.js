import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { configuration } from "./../config";
import { createMember } from "./../../functionWrite/db/members";
import { fetchRandomUser } from "../util/random-user";
import {deleteMember} from "./members"

describe("members db client - delete - tests suite: ", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
    it("should delete a member", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            member
        );
        expect(result.ok).toBeTruthy();

        const deleteResult = await deleteMember(
            {

                    client: dbClient,
                    config: {
                        ...configuration("dev"),
                        tableName: "test",
                    },
            },
        member.email, member.lastName
        );

        expect(deleteResult.ok).toBe(true);
        expect(deleteResult.deletedMember).toEqual(member);
    });

    it("should return ok: false with a reason of 'db-error' if unable to delete a member", async () => {
        const deleteResult = await deleteMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            "not-legit", "something-not-found"
        );

        expect(deleteResult.ok).toBe(false);
        expect(deleteResult.reason).toBe("db-error");
        expect(deleteResult.message).toBe("The conditional request failed");
    });

    it("should return ok: false with a reason if something bad happens during delete", async () => {
        const deleteResult = await deleteMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            "not-legit", ""
        );

        expect(deleteResult.ok).toBe(false);
        expect(deleteResult.reason).toBe("db-error");
        expect(deleteResult.message).toBeDefined();
    });
});
