import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";

export const deleteMember = async (deps, email, lastName) => {
    try {
        const documentDb = DynamoDBDocument.from(deps.client);
        const params = {
            Key: {
                email: email,
                lastName: lastName,
            },
            ConditionExpression: "email = :e and lastName = :ln",
            ExpressionAttributeValues: {
                ":e": email,
                ":ln": lastName,
            },
            ReturnValues: "ALL_OLD",
            TableName: deps.config.tableName,
        };

        const deleteResult = await documentDb.delete(params);

        if (
            deleteResult.$metadata.httpStatusCode === 200 &&
            deleteResult?.Attributes
        ) {
            return { ok: true, deletedMember: deleteResult?.Attributes };
        }
        return {
            ok: false,
            reason: "no-match",
            message: "There are no members matching the provided email and lastName.",
        };
    } catch (e) {
        console.log(`delete of member was unsuccessful: `);
        console.log(e);
        return { ok: false, reason: "db-error", message: e.message };
    }
};
