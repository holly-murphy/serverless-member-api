import { memberSchema } from "../util/member-schema.js";
import * as dbClient from "../db/members.js";

export const deleteMember = async (deps, email, lastName) => {
    const validationResult = memberSchema.validate({email, lastName}, {
        abortEarly: false,
    });
    if (validationResult?.error) {
        return {
            statusCode: 400,
            body: { message: validationResult.error.message },
        };
    }
    const deleteResult = await dbClient.deleteMember(deps, email, lastName);
    if (!deleteResult.ok) {
        return { statusCode: 500, message: deleteResult.message };
    }
    return {
        statusCode: 200,
        body: { deletedMember: deleteResult.deletedMember },
    };
};
