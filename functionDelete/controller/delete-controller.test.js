import {DynamoDBClient} from "@aws-sdk/client-dynamodb";
import { createMember} from "./../../functionWrite/controller/write-controller.js";
import {deleteMember} from "./delete-controller"
import {configuration} from "../../config.js";
import {fetchRandomUser} from "./../util/random-user.js";

describe("delete controller test suite", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});

    it("should delete a member", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            member
        );
        expect(result.statusCode).toBe(201);

        const deleteResult = await deleteMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            member.email, member.lastName
        );
        expect(deleteResult.statusCode).toBe(200);
        expect(deleteResult.body.deletedMember).toEqual(member);
    });

    it("should return a 400:bad-request if email or lastname is not provided", async () => {
        const deleteResult = await deleteMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            "", ""
        );
        expect(deleteResult.statusCode).toBe(400);
        expect(deleteResult.body.message).toBe(
            '"lastName" is not allowed to be empty. \"email\" is not allowed to be empty'
        );
    });
});
