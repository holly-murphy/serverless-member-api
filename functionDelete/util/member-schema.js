import Joi from "joi";

export const memberSchema = Joi.object({
    lastName: Joi.string().required(),
    email: Joi.string().required(),
});
