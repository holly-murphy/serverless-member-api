import {DynamoDBClient} from "@aws-sdk/client-dynamodb"
import * as membersController from "./controller/delete-controller.js"

export const handler = async (event) => {
    const config = {tableName: "Members"}
    let statusCode, body, lastName, email;
    try {
        const client = new DynamoDBClient({});
        const deps = {config, client};
        lastName = event.queryStringParameters.lastName;
        email = event.queryStringParameters.email;

        const result = await membersController.deleteMember(
            deps,
            email, lastName
        );
        statusCode = result.statusCode
        body = JSON.stringify(result.body)
    } catch (err) {
        statusCode = 500;
        body = err.message;
    }
    return {
        "isBase64Encoded": false,
        statusCode,
        body,
        "headers": {"content-type":"application/json"},
    }
};
