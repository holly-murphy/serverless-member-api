import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";

export const getMemberByEmailAndLastName = async (deps, memberDetails) => {
    try {
        const documentDb = DynamoDBDocument.from(deps.client);
        const params = {
            Key: {
                email: memberDetails.email,
                lastName: memberDetails.lastName,
            },
            TableName: deps.config.tableName,
        };

        const getResult = await documentDb.get(params);
        if (getResult.$metadata.httpStatusCode === 200 && getResult?.Item) {
            return { ok: true, member: getResult?.Item };
        }
        return {
            ok: false,
            reason: "no-match",
            message: "There are no members matching the provided email and lastName.",
        };
    } catch (e) {
        console.log(`fetch of member was unsuccessful: `);
        console.log(e);
        return { ok: false, reason: "db-error", message: e.message };
    }
};