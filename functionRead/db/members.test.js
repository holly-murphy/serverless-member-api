import {DynamoDBClient} from "@aws-sdk/client-dynamodb";
import {configuration} from "./../config";
import {getMemberByEmailAndLastName} from "./members";
import {createMember} from "./../../functionWrite/db/members"
import {fetchRandomUser} from "../util/random-user";

describe("members db client - get - tests suite: ", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});
    const tableName = "test";

    it("should get a member by email and lastName", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    tableName: `test`,
                },
            },
            member
        );

        const queryResult = await getMemberByEmailAndLastName(
            {
                client: dbClient,
                config: {
                    tableName: `test`,
                },
            },
            member
        );

        expect(queryResult.ok).toBe(true);
        expect(queryResult.member).toEqual(member);
    });

    it("should return ok: false with a reason of 'no-match' if unable to fetch a member", async () => {
        const result = await getMemberByEmailAndLastName(
            {
                client: dbClient,
                config: {
                    tableName: `test`,

                },
            },
            {email: "not-correct", lastName: "also-not-legit"}
        );

        expect(result.ok).toBe(false);
        expect(result.reason).toBe("no-match");
        expect(result.message).toBeDefined();
    });

    it("should return ok: false with a reason if something bad happens during fetch", async () => {
        const result = await getMemberByEmailAndLastName(
            {

                client: dbClient,
                config: {
                    tableName: `test`,
                },
            },
            {}
        );

        expect(result.ok).toBe(false);
        expect(result.reason).toBe("db-error");
        expect(result.message).toBeDefined();
    });
});
