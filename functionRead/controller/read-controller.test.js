import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import {
    getMemberByEmailAndLastName,
} from "./read-controller.js";
import {createMember} from "../../functionWrite/db/members";
import { DB } from "../../DB.js";
import { configuration } from "../../config.js";
import { fetchRandomUser } from "./../util/random-user.js";

describe("get controller test suite", () => {
    const dbClient = new DynamoDBClient({endpoint: "http://localhost:8000",});

    it("should get a member by email and last name", async () => {
        const member = await fetchRandomUser()
        const result = await createMember(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
             member
        );
        expect(result.ok).toBeTruthy();

        const getResult = await getMemberByEmailAndLastName(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            { lastName: member.lastName, email: member.email }
        );
        expect(getResult.statusCode).toBe(200);
        expect(getResult.body.member).toEqual(member);
    });

    it("should not find a member, if criteria doesn't match what is in the db", async () => {
        const member = await fetchRandomUser()
        const getResult = await getMemberByEmailAndLastName(
            {
                client: dbClient,
                config: {
                    ...configuration("dev"),
                    tableName: "test",
                },
            },
            { ...member, email: "notindb@test.com" }
        );
        expect(getResult.statusCode).toBe(500);
        expect(getResult.body.message).toBe(
            "There are no members matching the provided email and lastName."
        );
    });
   });
