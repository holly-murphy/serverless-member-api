import { memberSchema } from "../util/member-schema.js";
import * as dbClient from "../db/members.js";

export const getMemberByEmailAndLastName = async (deps, member) => {
    const validationResult = memberSchema.validate(member, {
        abortEarly: false,
    });
    if (validationResult?.error) {
        return {
            statusCode: 400,
            body: { message: validationResult.error.message },
        };
    }
    const getResult = await dbClient.getMemberByEmailAndLastName(deps, member);
    if (!getResult.ok) {
        return { statusCode: 500, body: { message: getResult.message } };
    }
    return { statusCode: 200, body: { member: getResult.member } };
};
