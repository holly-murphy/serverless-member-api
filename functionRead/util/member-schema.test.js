import { memberSchema } from "./member-schema";
import { fetchRandomUser } from "./random-user";

describe("memberSchema test suite: ", () => {
    let member;
    beforeAll(async () => {
        member = await fetchRandomUser();
    });

    it("should validate the passed object is a member and return the values", () => {
        const result = memberSchema.validate(member);
        expect(result).toEqual({
            value: {
                ...member,
            },
        });
    });

    it("should require the firstName to be a string", () => {
        const result = memberSchema.validate({ ...member, firstName: 123 });
        expect(result.error.message).toBe('"firstName" must be a string');
    });

    it("should require a firstName and lastName", () => {
        const noName = { ...member };
        delete noName.firstName;
        delete noName.lastName;

        const result = memberSchema.validate(noName, { abortEarly: false });
        expect(result.error.message).toBe('"lastName" is required');
    });

    it("should validate the email is valid", () => {
        const result = memberSchema.validate({ ...member, email: "123xxx234" });
        expect(result.error.message).toBe('"email" must be a valid email');
    });

    it("should validate the gender is one provided", () => {
        const result = memberSchema.validate({ ...member, gender: "123xxx234" });
        expect(result.error.message).toBe(
            '"gender" must be one of [male, female, na, null, ]'
        );
    });
});
