import Joi from "joi";

const genderEnum = ["male", "female", "na"];
const lastNameSchema = Joi.string().required();
const emailSchema = Joi.string()
    .email({ tlds: { allow: false } })
    .required();
const genderSchema = Joi.string()
    .valid(...genderEnum)
    .insensitive().allow(null, "");

export const memberSchema = Joi.object({
    firstName: Joi.string().allow(null, ""),
    lastName: lastNameSchema,
    middleInitial: Joi.string().allow(null, ""),
    phone: Joi.string().allow(null, ""),
    email: emailSchema,
    gender: genderSchema
});